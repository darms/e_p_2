/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jogo;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author darms
 */
public class Abrindo_Arquivos extends JFrame {
    
    private Acoes canvas = new Acoes();
    private CanvasThread updateScreenThread = new CanvasThread(canvas);
    
    public int matriz=0;
    private int tipo=1;
    private int quantidade_deBarcos=0;
    
    public Abrindo_Arquivos(){
        
    }
    
    void Abre_Arquivo(){
        JFileChooser selecao = new JFileChooser();
        int returnVal = selecao.showOpenDialog(this);
        int contador=0;
        if(returnVal == JFileChooser.APPROVE_OPTION){
        File file = selecao.getSelectedFile();
        
        try {
            //Inicio da leitura do arquivo.
            FileReader arquivo = new FileReader( file.getAbsolutePath() );
            BufferedReader lerArquivo = new BufferedReader(arquivo);
            
            int i=0,k=0,numero_de_barcos;
            int Largura=0,Altura = 0;
            String linha = lerArquivo.readLine(); 
           
            contador++;
            // lê a primeira linha
            // a variável "linha" recebe o valor "null" quando o processo
            // de repetição atingir o final do arquivo texto
            while (linha != null) {
                
                
                //Pegando largura e Altura
                if(contador==2){
                    String[] textoSeparado = linha.split(" ");
                    Largura=Integer.parseInt(textoSeparado[0]);
                    Altura=Integer.parseInt(textoSeparado[1]);
                    canvas.setNumColunas(Largura);
                    canvas.setNumLinhas(Altura);
                    
                    
                }
                //Pegando os valores da Matriz
                if(contador>=5 && contador<5+Altura){
                    
                    char[] arrayValores = linha.toCharArray();
                    
                        
                        for(int j=0;j<arrayValores.length;j++){
                            this.matriz=Integer.parseInt(String.valueOf(arrayValores[j]));
                            canvas.setMatriz(j,i,this.matriz);
                            
                            
                        }
                            
                        
                        i++;
                    
                    
                }
               //Pegando a quantidade de barcos de cada tipo.
                if(contador>=Altura+7){
                    String barcos[] = linha.split(" ");
                    numero_de_barcos=Integer.parseInt(barcos[1]);
                    
                    quantidade_deBarcos+=numero_de_barcos;
                    
                    
                    
                    
                }
                
                linha = lerArquivo.readLine(); // lê da segunda até a última linha
                contador++;
            
            }
            //setando o numero de usos de acda poder.
            canvas.setQuant_deBarcos(quantidade_deBarcos);
            canvas.setContadorTiro((canvas.getNumLinhas()*canvas.getNumColunas()/2));
            canvas.setContadorBala_deCanhao(3);
            canvas.setContadorDetector(2);
            canvas.setContadorMisselLinha(1);
            canvas.setContadorMisselColuna(1);
            
            arquivo.close();
            
            
            } catch (IOException ex) {
            System.out.println("problem accessing file"+file.getAbsolutePath());
            }
            
        }    
        //se o mapa tiver sido escolhido entrará no if abaixo.
        if(contador>0){
             setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Centralizar Janela
		setLocationRelativeTo(null);
		
		getContentPane().setLayout(new BorderLayout());
		setTitle("Batalha Naval");
		getContentPane().add(canvas,BorderLayout.CENTER);
		
		// Define largura e altura da janela principal
		setSize((canvas.Largura_Canvas) * canvas.getNumColunas(), (canvas.Altura_Canvas * canvas.getNumLinhas())+50);
           
           //contrução dos botoes dos poderes e do botao dos pontos
            JButton botaoTiro = new JButton("Tiro x"+canvas.getContadorTiro());
            JButton botaoBala_deCanhao = new JButton("BaladeCanhao x"+canvas.getContadorBala_deCanhao());  
            JButton botaoDetector = new JButton("Detector x"+canvas.getContadorDetector());
            JButton botaoMisselLinha = new JButton("MisselLinha x"+canvas.getContadorMisselLinha());
            JButton botaoMisselColuna = new JButton("MisselColuna x"+canvas.getContadorMisselColuna());
            
            JPanel panel = new JPanel();
            
            JLabel pontos = new JLabel("Pontos: "+canvas.getPontos());
            
            panel.add(botaoTiro);
            panel.add(botaoBala_deCanhao); 
            panel.add(botaoDetector);   
            panel.add(botaoMisselLinha);
            panel.add(botaoMisselColuna);
            panel.add(pontos);
            
            getContentPane().add(panel, BorderLayout.SOUTH);
	
            
            
            setVisible(true);   
            updateScreenThread.start();
            
             botaoTiro.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    botaoTiroActionPerformed(evt);
                }
            });
            
            botaoBala_deCanhao.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    botaoBala_deCanhaoActionPerformed(evt);
                }
            });
            
            botaoDetector.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    botaoDetectorActionPerformed(evt);
                }
            });
            
            botaoMisselLinha.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    botaoMisselLinhaActionPerformed(evt);
                }
            });
           
            botaoMisselColuna.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    botaoMisselColunaActionPerformed(evt);
                }
            });
            
            
            
                canvas.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
		        int x=e.getX();
		        int y=e.getY();
				
		        int x_pos = x/canvas.Largura_Canvas;
		        int y_pos = y/canvas.Altura_Canvas;

		        canvas.setTiro(getTipo(),x_pos, y_pos);
                        
                         pontos.setText("Pontos: "+ canvas.getPontos() );
                    
                        botaoTiro.setText("Tiro x"+canvas.getContadorTiro());
                        botaoBala_deCanhao.setText("BaladeCanhao x"+canvas.getContadorBala_deCanhao());
                        botaoDetector.setText("Detector x"+canvas.getContadorDetector());
                        botaoMisselLinha.setText("MisselLinha x"+canvas.getContadorMisselLinha());
                        botaoMisselColuna.setText("MisselColuna x"+canvas.getContadorMisselColuna());
				
			}
                         
                        
			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}


		});
        }
        
        //caso aperte o botao cancelar.
        else {
            System.out.println("File access cancelled by user.");
        }
          
    }
    //diferenciando os tipos de ações.
    private void botaoTiroActionPerformed(java.awt.event.ActionEvent evt){
        this.tipo = 1;
    }
    
    private void botaoBala_deCanhaoActionPerformed(java.awt.event.ActionEvent evt){
        this.tipo = 2;
    }
    
    private void botaoDetectorActionPerformed(java.awt.event.ActionEvent evt){
        this.tipo = 3;
    }        

    private void botaoMisselLinhaActionPerformed(java.awt.event.ActionEvent evt){
        this.tipo = 4;
    }
    
    private void botaoMisselColunaActionPerformed(java.awt.event.ActionEvent evt){
        this.tipo = 5;
    }
    
    public int getTipo(){
        return this.tipo;
    }
    
}
