/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jogo;

import javax.swing.JOptionPane;

/**
 *
 * @author darms
 */
public class Acoes extends CanvasJogo{
    
    
    
    private int contTiro;
    private int contBala_deCanhao;
    private int contDetector;
    private int contMisselLinha;
    private int contMisselColuna;
    
    private int pontos;
    private int quant_deBarcos;
    
    
     int quant_deAcertos=0;
   
    
    public void setTiro(int tipo,int x, int y) {
       
        //Verificação da destruição dos barcos. A ideia foi percorrer a matriz para ver se algo novo foi destruido.
        
        if( (contTiro>0 || contBala_deCanhao>0 )&& quant_deAcertos<getQuant_deBarcos()){
           
            //Acão do tipo Tiro
            if(tipo==1 && contTiro>0){
                
                if(getMatriz(x,y) == 0 || getMatriz(x,y) == 12){
                    setMatriz(x,y,6);
                    contTiro--; 
                    perdePontos();
                }
                else if(getMatriz(x,y) == 6){
                    setMatriz(x,y,6);
                }
                else if(getMatriz(x,y)== 1 || getMatriz(x,y)==2 || getMatriz(x,y)==3 || getMatriz(x,y)==4 || getMatriz(x,y)==5){
                    //Tiro[x][y] = 7;
                    contTiro--;
                    ganhaPontos();
                    if(getMatriz(x,y)==1){
                        setMatriz(x,y,7);
                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 1");
                        quant_deAcertos++;
                    }
                   
                    if(getMatriz(x,y)==2 ){
                        setMatriz(x,y,8);
                        
                        
                        if(x-1<0 && y-1<0){
                            if(getMatriz(x+1,y)==8 || getMatriz(x,y+1)==8){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 2");
                                quant_deAcertos++;
                            }    
                        }
                        
                        
                        if(x-1<0){
                            if((getMatriz(x+1,y)==8 || getMatriz(x,y+1)==8 || getMatriz(x,y-1)==8)  ){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 2");
                                quant_deAcertos++;
                            }    
                        }
                        
                        else if(y-1<0){
                            if((getMatriz(x+1,y)==8 || getMatriz(x-1,y)==8 || getMatriz(x,y+1)==8)  ){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 2");
                                quant_deAcertos++;
                                }   
                        }
                        
                        else{
                            if((getMatriz(x+1,y)==8 || getMatriz(x-1,y)==8 || getMatriz(x,y+1)==8 || getMatriz(x,y-1)==8)  ){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 2");
                                quant_deAcertos++;
                            }   
                        }
                        
                        
                    }
                    
                    if(getMatriz(x,y)==3){
                        setMatriz(x,y,9); 
                        
                        for(int i=y;i<=getNumLinhas();i++){
                            if(y-1<0){
                           
                                if(getMatriz(x,i)==9 && getMatriz(x,i+1)==9 && getMatriz( x,i+2)==9){
                                    JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                    quant_deAcertos++;
                                    break;
                                }
                        
                            }
                            
                            else if(y-2<0){
                                if(getMatriz(x,i)==9 && getMatriz(x,i+1)==9 && getMatriz(x,i+2)==9){
                                    JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                    quant_deAcertos++;
                                    break;
                                }
                                if(getMatriz(x,i)==9 && getMatriz(x,i+1)==9 && getMatriz(x,i-1)==9){
                                    JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                    quant_deAcertos++;
                                    break;
                                }
                            }
                            
                            else{
                                
                        
                                if(getMatriz(x,i)==9 && getMatriz(x,i+1)==9 && getMatriz(x,i+2)==9){
                                    JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                    quant_deAcertos++;
                                    break;
                                }
                            
                                else if(getMatriz(x,i)==9 && getMatriz(x,i+1)==9 && getMatriz(x,i-1)==9){
                                    JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                    quant_deAcertos++;
                                    break;
                                }
                            
                                else if(getMatriz(x,i)==9 && getMatriz(x,i-1)==9 && getMatriz(x,i-2)==9){  
                                    JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                    quant_deAcertos++;
                                    break;
                                }
                            }
                        
                        
                        }
                        for(int i=x;i<=getNumColunas();i++){
                            if(x-1<0){
                                if(getMatriz(i,y)==9 && getMatriz(i+1,y)==9 &&  getMatriz(i+2,y)==9){
                                    JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                    quant_deAcertos++;
                                    break;
                                }   
                            }
                            
                            if(x-2<0){
                                if(getMatriz(i,y)==9 && getMatriz(i+1,y)==9 &&  getMatriz(i+2,y)==9){
                                    JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                    quant_deAcertos++;
                                    break;
                                }   
                                
                                else if(getMatriz(i,y)==9 && getMatriz(i+1,y)==9 && getMatriz(i-1,y)==9){
                                    JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                    quant_deAcertos++;
                                    break;
                                }    
                                        
                                
                                
                            }
                            
                            else{
                            
                                if(getMatriz( i,y)==9 && getMatriz( i+1,y)==9 &&  getMatriz(i+2,y)==9){
                                    JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                    quant_deAcertos++;
                                    break;
                                }
                        
                                else if(getMatriz(i,y)==9 && getMatriz(i+1,y)==9 && getMatriz(i-1,y)==9){
                                    JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                    quant_deAcertos++;
                                    break;
                                }        
                                        
                                else if(getMatriz(i,y)==9 && getMatriz(i-1,y)==9 && getMatriz(i-2,y)==9){  
                                    JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                    quant_deAcertos++;
                                    break;
                                }
                            } 
                        }
                        
                        
                        
                        
                       
                      
                    }
                    
                
                    if(getMatriz(x,y)==4){
                        setMatriz(x,y,10);
                       
                        
                        for(int i=y;i<=getNumLinhas();i++){
                            
                 
                            if(getMatriz(x,i)==10 && getMatriz(x,i+1)==10 && getMatriz(x,i+2)==10 && getMatriz(x,i+3)==10 ){  
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                quant_deAcertos++;
                                break;
                            }
                                
                            else if(getMatriz(x,i)==10 &&getMatriz(x,i+1)==10 && getMatriz( x,i-1)==10 && getMatriz(x,i+2)==10){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                quant_deAcertos++;
                                break;
                                }
                                
                            else if(getMatriz( x,i)==10 && getMatriz( x,i+1)==10 && getMatriz( x,i-1)==10 && getMatriz(x,i-2)==10){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                quant_deAcertos++;
                                break;
                            }
                                
                            else if(getMatriz(x,i)==10 &&getMatriz(x,i-1)==10 && getMatriz(x,i-2)==10 && getMatriz(x,i-3)==10){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                quant_deAcertos++;
                                break;
                            }
                            
                        }
                        for(int i=x;i<=getNumColunas();i++){
                     
                            if(getMatriz(i,y)==10 && getMatriz(i+1,y)==10 && getMatriz(i+2,y)==10 && getMatriz(i+3,y)==10 ){  
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                quant_deAcertos++;
                                break;
                            }
                                
                            else if(getMatriz(i,y)==10 && getMatriz(i+1,y)==10 && getMatriz(i-1,y)==10 && getMatriz(i+2,y)==10){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                quant_deAcertos++;
                                break;
                                }
                                
                            else if(getMatriz(i,y)==10 && getMatriz(i+1,y)==10 && getMatriz(i-1,y)==10 && getMatriz(i-2,y)==10){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                quant_deAcertos++;
                                break;
                            }
                     
                            else if(getMatriz(i,y)==10 && getMatriz(i-1,y)==10 && getMatriz(i-2,y)==10 && getMatriz(i-3,y)==10 ){  
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                quant_deAcertos++;
                                break;
                            }
                        }
                            
             
                   
                    
                    }
                
                    
                    if(getMatriz(x,y)==5){
                        setMatriz(x,y,11);
                       
                        
                        for(int i=y;i<=getNumLinhas();i++){
                            
                            
                            if(getMatriz(x,i)==11 && getMatriz(x,i+1)==11 && getMatriz(x,i+2)==11 && getMatriz(x,i+3)==11 && getMatriz(x,i+4)==11 ){  
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                quant_deAcertos++;
                                break;
                            }
  
                            else if(getMatriz(x,i)==11 && getMatriz(x,i+1)==11 && getMatriz(x,i-1)==11 && getMatriz(x,i+2)==11 && getMatriz(x,i+3)==11){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                quant_deAcertos++;
                                break;
                            }
                            
                            else if(getMatriz(x,i)==11 && getMatriz(x,i+1)==11 && getMatriz(x,i-1)==11 && getMatriz(x,i+2)==11 && getMatriz( x,i-2)==11){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                quant_deAcertos++;
                                break;
                            }
                            
                            else if(getMatriz(x,i)==11 && getMatriz(x,i+1)==11 && getMatriz( x,i-1)==11 && getMatriz(x,i-2)==11 && getMatriz(x,i-3)==11){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                quant_deAcertos++;
                                break;
                            }
                        
                            else if(getMatriz( x,i)==11 && getMatriz( x,i-1)==11 && getMatriz( x,i-2)==11 && getMatriz( x,i-3)==11 && getMatriz( x,i-4)==11){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                quant_deAcertos++;
                                break;
                            }
                        }
                        
                        for(int i=x;i<=getNumColunas();i++){
                           
                       
                            if(getMatriz(i,y)==11 && getMatriz(i+1,y)==11 && getMatriz(i+2,y)==11 && getMatriz(i+3,y)==11 && getMatriz( i+4,y)==11){  
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                quant_deAcertos++;
                                break;
                            }
                        
                            else if(getMatriz(i,y)==11 && getMatriz(i+1,y)==11 && getMatriz(i-1,y)==11 && getMatriz(i+2,y)==11 && getMatriz(i+3,y)==11){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                quant_deAcertos++;
                                break;
                            }
                            
                            else if(getMatriz(i,y)==11 && getMatriz(i+1,y)==11 && getMatriz(i-1,y)==11 && getMatriz(i+2,y)==11 && getMatriz(i-2,y)==11){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                quant_deAcertos++;
                                break;
                            }
                            
                            else if(getMatriz(i,y)==11 && getMatriz(i+1,y)==11 && getMatriz(i-1,y)==11 && getMatriz(i-2,y)==11 && getMatriz(i-3,y)==11){
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                quant_deAcertos++;
                                break;
                            }
                            
                            else if(getMatriz(i,y)==11 && getMatriz(i-1,y)==11 && getMatriz(i-2,y)==11 && getMatriz( i-3,y)==11 && getMatriz(i-4,y)==11){  
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                quant_deAcertos++;
                                break;
                            }
                            
                            
                            
                        }
                    
                    
                    
                    }
                    
                }    
            }  
            //Acao do tipo Bala_de_Canhao
            else if(tipo==2 && contBala_deCanhao > 0 &&((getMatriz(x,y)!=6 && getMatriz(x,y)!=7 && getMatriz(x,y)!=8 && getMatriz(x,y)!=9 && getMatriz(x,y)!=10 && getMatriz(x,y)!=11)
                    ||(getMatriz(x+1,y)!=6 && getMatriz(x+1,y)!=7 && getMatriz(x+1,y)!=8 && getMatriz(x+1,y)!=9 && getMatriz(x+1,y)!=10 && getMatriz(x+1,y)!=11)  
                    ||(getMatriz(x,y+1)!=6 && getMatriz(x,y+1)!=7 && getMatriz(x,y+1)!=8 && getMatriz(x,y+1)!=9 && getMatriz(x,y+1)!=10 && getMatriz(x,y+1)!=11)
                    ||(getMatriz(x+1,y+1)!=6 && getMatriz(x+1,y+1)!=7 && getMatriz(x+1,y+1)!=8 && getMatriz(x+1,y+1)!=9 && getMatriz(x+1,y+1)!=10 && getMatriz(x+1,y+1)!=11))){
            contBala_deCanhao--;   
                 
            
                for(int m=0;m<2;m++){
                    for(int n=0;n<2;n++){
                            
                        if((getMatriz(x+m, y+n) == 0)||(getMatriz(x+m, y+n)==12)){
                            setMatriz(x+m, y+n, 6);
                        }  
            
                        else {
                            if(getMatriz(x+m,y+n)==1){
                                setMatriz(x+m, y+n, 7);  
                                JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 1");
                                quant_deAcertos++;
                            }
                            
                    
                            else if(getMatriz(x+m,y+n)==2){
                                setMatriz(x+m, y+n, 8);  
                            
                                if(x+m-1<0 && y+n-1<0){
                                    if(getMatriz(x+1+m,y+n)==8 || getMatriz(x+m,y+n+1)==8){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 2");
                                        quant_deAcertos++;
                                    }    
                                }
                        
                        
                                if(x-1+m<0){
                                    if((getMatriz(x+1+m,y+n)==8 || getMatriz(x+m,y+1+n)==8 || getMatriz(x+m,y-1+n)==8)  ){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 2");
                                        quant_deAcertos++;
                                    }    
                                }
                        
                                else if(y-1+n<0){
                                    if((getMatriz(x+1+m,y+n)==8 || getMatriz(x-1+m,y+n)==8 || getMatriz(x+m,y+1+n)==8)  ){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 2");
                                        quant_deAcertos++;
                                    }   
                                }
                        
                                else{
                                    if((getMatriz(x+1+m,y+n)==8 || getMatriz(x-1+m,y+n)==8 || getMatriz(x+m,y+1+m)==8 || getMatriz(x+m,y-1+n)==8)  ){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 2");
                                        quant_deAcertos++;
                                    }   
                                }
                            
                            }
                    
                            else if(getMatriz(x+m,y+n)==3){
                                setMatriz(x+m, y+n, 9);  
                                for(int i=y+n;i<=getNumLinhas();i++){
                                    if(y+m-1<0){
                           
                                        if(getMatriz(x+m,i+m)==9 && getMatriz(x+m,i+1+n)==9 && getMatriz( x+m,i+2+n)==9){
                                            JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                            quant_deAcertos++;
                                            break;
                                        }
                        
                                    }
                            
                                    else if(y-2+n<0){
                                        if(getMatriz(x+m,i+n)==9 && getMatriz(x+m,i+1+n)==9 && getMatriz(x+m,i+2+n)==9){
                                            JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                            quant_deAcertos++;
                                            break;
                                        }
                                
                                        if(getMatriz(x+m,i+n)==9 && getMatriz(x+m,i+1+n)==9 && getMatriz(x+m,i-1+n)==9){
                                            JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                            quant_deAcertos++;
                                            break;
                                        }
                                    }
                            
                                    else{
                                
                        
                                        if(getMatriz(x+m,i+n)==9 && getMatriz(x+m,i+1+n)==9 && getMatriz(x+m,i+2+n)==9){
                                            JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                            quant_deAcertos++;
                                            break;
                                        }
                            
                                        else if(getMatriz(x+m,i+n)==9 && getMatriz(x+m,i+1+n)==9 && getMatriz(x+m,i-1+n)==9){
                                            JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                            quant_deAcertos++;
                                            break;
                                        }
                            
                                        else if(getMatriz(x+m,i+n)==9 && getMatriz(x+m,i-1+n)==9 && getMatriz(x+m,i-2+n)==9){  
                                            JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                            quant_deAcertos++;
                                            break;
                                        }
                                    }
                        
                        
                                }
                                for(int i=x+m;i<=getNumColunas();i++){
                                    if(x+m-1<0){
                                        if(getMatriz(i+m,y+n)==9 && getMatriz(i+1+m,y+n)==9 &&  getMatriz(i+2+m,y+n)==9){
                                            JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                            quant_deAcertos++;
                                            break;
                                        }   
                                    }
                            
                                    if(x-2+m<0){
                                        if(getMatriz(i+m,y+n)==9 && getMatriz(i+1+m,y+n)==9 &&  getMatriz(i+2+m,y+n)==9){
                                            JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                            quant_deAcertos++;
                                            break;
                                        }   
                                
                                        else if(getMatriz(i+m,y+n)==9 && getMatriz(i+1+m,y+n)==9 && getMatriz(i-1+m,y+n)==9){
                                            JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                            quant_deAcertos++;
                                            break;
                                        }    
                                    }
                            
                                    else{
                            
                                        if(getMatriz( i+m,y+n)==9 && getMatriz( i+1+m,y+n)==9 &&  getMatriz(i+2+m,y+n)==9){
                                            JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                            quant_deAcertos++;
                                            break;
                                        }
                        
                                        else if(getMatriz(i+m,y+n)==9 && getMatriz(i+1+m,y+n)==9 && getMatriz(i-1+m,y+n)==9){
                                            JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                            quant_deAcertos++;
                                            break;
                                        }        
                                        
                                        else if(getMatriz(i+m,y+n)==9 && getMatriz(i-1+m,y+n)==9 && getMatriz(i-2+m,y+n)==9){  
                                            JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 3");
                                            quant_deAcertos++;
                                            break;
                                        }
                                    } 
                                }
                            
                            }
                    
                            else if(getMatriz(x+m,y+n)==4){
                                setMatriz(x+m, y+n, 10);  
                                
                                for(int i=y+n;i<=getNumLinhas();i++){
                            
                                    if(getMatriz(x+m,i+n)==10 && getMatriz(x+m,i+1+n)==10 && getMatriz(x+m,i+2+n)==10 && getMatriz(x+m,i+3+n)==10){  
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                        quant_deAcertos++;
                                        break;
                                    }
  
                                    else if(getMatriz(x+m,i+n)==10 && getMatriz(x+m,i+1+n)==10 && getMatriz(x+m,i-1+n)==10 && getMatriz(x+m,i+2+n)==10){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                        quant_deAcertos++;
                                        break;
                                    }
                            
                                    else if(getMatriz(x+m,i+n)==10 && getMatriz(x+m,i+1+n)==10 && getMatriz(x+m,i-1+n)==10 && getMatriz(x+m,i-2+n)==10 ){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                        quant_deAcertos++;
                                        break;
                                    }
                            
                                    else if(getMatriz(x+m,i+n)==10 && getMatriz(x+m,i-1+n)==10 && getMatriz(x+m,i-2+n)==10 && getMatriz(x+m,i-3+n)==10){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                        quant_deAcertos++;
                                        break;
                                    }
                                }
                        
                            
                                for(int i=x+m;i<=getNumColunas();i++){
                   
                                    if(getMatriz(i+m,y+n)==10 && getMatriz(i+m+1,y+n)==10 && getMatriz(i+2+m,y+n)==10 && getMatriz(i+3+m,y+n)==10){  
                               
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                        quant_deAcertos++;
                                        break;
                                    }
                        
                                    else if(getMatriz(i+m,y+n)==10 && getMatriz(i+1+m,y+n)==10 && getMatriz(i-1+m,y+n)==10 && getMatriz(i+2+m,y+n)==10){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                        quant_deAcertos++;
                                        break;
                                    }
                            
                                    else if(getMatriz(i+m,y+n)==10 && getMatriz(i+1+m,y+n)==10 && getMatriz(i-1+m,y+n)==10 && getMatriz(i-2+m,y+n)==10){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                        quant_deAcertos++;
                                        break;
                                    }
                            
                                    else if(getMatriz(i+m,y+n)==10 && getMatriz(i-1+m,y+n)==10 && getMatriz(i-2+m,y+n)==10 && getMatriz( i-3+m,y+n)==10){  
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 4");
                                        quant_deAcertos++;
                                        break;
                                    }
                   
                                }
                            
                            
                            
                            
                            
                            }
                    
                            else if(getMatriz(x+m,y+n)==5){
                            setMatriz(x+m, y+n, 11);  
                                    
                                for(int i=y+n;i<=getNumLinhas();i++){
                            
                                    if(getMatriz(x+m,i+n)==11 && getMatriz(x+m,i+1+n)==11 && getMatriz(x+m,i+2+n)==11 && getMatriz(x+m,i+3+n)==11 && getMatriz(x+m,i+4+n)==11 ){  
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                        quant_deAcertos++;
                                        break;
                                    }
  
                                    else if(getMatriz(x+m,i+n)==11 && getMatriz(x+m,i+1+n)==11 && getMatriz(x+m,i-1+n)==11 && getMatriz(x+m,i+2+n)==11 && getMatriz(x+m,i+3+n)==11){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                        quant_deAcertos++;
                                        break;
                                    }
                            
                                    else if(getMatriz(x+m,i+n)==11 && getMatriz(x+m,i+1+n)==11 && getMatriz(x+m,i-1+n)==11 && getMatriz(x+m,i+2+n)==11 && getMatriz( x+m,i-2+n)==11){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                        quant_deAcertos++;
                                        break;
                                    }
                            
                                    else if(getMatriz(x+m,i+n)==11 && getMatriz(x+m,i+1+n)==11 && getMatriz( x+m,i-1+n)==11 && getMatriz(x+m,i-2+n)==11 && getMatriz(x+m,i-3+n)==11){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                        quant_deAcertos++;
                                        break;
                                    }
                        
                                    else if(getMatriz(x+m,i+n)==11 && getMatriz(x+m,i-1+n)==11 && getMatriz(x+m,i-2+n)==11 && getMatriz(x+m,i-3+n)==11 && getMatriz( x+m,i-4+n)==11){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                        quant_deAcertos++;
                                        break;
                                    }
                                }
                        
                            
                                for(int i=x+m;i<=getNumColunas();i++){
                   
                                    if(getMatriz(i+m,y+n)==11 && getMatriz(i+m+1,y+n)==11 && getMatriz(i+2+m,y+n)==11 && getMatriz(i+3+m,y+n)==11 && getMatriz( i+4+m,y+n)==11){  
                               
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                        quant_deAcertos++;
                                        break;
                                    }
                        
                                    else if(getMatriz(i+m,y+n)==11 && getMatriz(i+m+1,y+n)==11 && getMatriz(i-1+m,y+n)==11 && getMatriz(i+2+m,y+n)==11 && getMatriz(i+3+m,y+n)==11){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                        quant_deAcertos++;
                                        break;
                                    }
                            
                                    else if(getMatriz(i+m,y+n)==11 && getMatriz(i+1+m,y+n)==11 && getMatriz(i-1+m,y+n)==11 && getMatriz(i+2+m,y+n)==11 && getMatriz(i-2+m,y+n)==11){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                        quant_deAcertos++;
                                        break;
                                    }
                            
                                    else if(getMatriz(i+m,y+n)==11 && getMatriz(i+1+m,y+n)==11 && getMatriz(i-1+m,y+n)==11 && getMatriz(i-2+m,y+n)==11 && getMatriz(i-3+m,y+n)==11){
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                        quant_deAcertos++;
                                        break;
                                    }
                            
                                    else if(getMatriz(i+m,y+n)==11 && getMatriz(i-1+m,y+n)==11 && getMatriz(i-2+m,y+n)==11 && getMatriz( i-3+m,y+n)==11 && getMatriz(i-4+m,y+n)==11){  
                                        JOptionPane.showMessageDialog(null,"Você destruiu um barco de tamanho 5");
                                        quant_deAcertos++;
                                        break;
                                    }
                   
                                }
                            
                            }
                    
                        }
                    }    
                }            
            
            }
           /* Tentativa de colocar o botao do detector falha.
            else if(tipo == 3 && contDetector > 0 &&( (getMatriz(x, y) != 6 && getMatriz(x, y) != 7 && getMatriz(x, y) != 8 && getMatriz(x, y) != 9 && getMatriz(x, y) != 10 && getMatriz(x, y) != 11 && getMatriz(x, y) != 12 &&  getMatriz(x, y) != 13)
                    || (getMatriz(x+1, y) != 6 && getMatriz(x+1, y) != 7 && getMatriz(x+1, y) != 8 && getMatriz(x+1, y) != 9 && getMatriz(x+1, y) != 10 && getMatriz(x+1, y) != 11 && getMatriz(x+1, y) != 12 &&  getMatriz(x+1, y) != 13)
                    ||(getMatriz(x, y+1) != 6 && getMatriz(x, y+1) != 7 && getMatriz(x, y+1) != 8 && getMatriz(x, y+1) != 9 && getMatriz(x, y+1) != 10 && getMatriz(x, y+1) != 11 && getMatriz(x, y+1) != 12 &&  getMatriz(x, y+1) != 13)
                    ||(getMatriz(x+1, y+1) != 6 && getMatriz(x+1, y+1) != 7 && getMatriz(x+1, y+1) != 8 && getMatriz(x+1, y+1) != 9 && getMatriz(x+1, y+1) != 10 && getMatriz(x+1, y+1) != 11 && getMatriz(x+1, y+1) != 12 &&  getMatriz(x+1, y+1) != 13))){
                
                
                
            }
           */ 
        }    
            else{
               
                
                if(getQuant_deBarcos()==quant_deAcertos){
                    JOptionPane.showMessageDialog(null,"Você ganhou");
                }
                else{
                    JOptionPane.showMessageDialog(null,"Você perdeu");
                }
               
                System.exit(0);
            }
    
        }  
    
    public void setContadorTiro(int quantidade){
        this.contTiro= quantidade;
    }
    
   public int getContadorTiro(){
       return this.contTiro;
    }
    public void setContadorBala_deCanhao(int quantidade){
        this.contBala_deCanhao = quantidade;
    }
    
    public int getContadorBala_deCanhao(){
        return this.contBala_deCanhao;
    }
    public void setContadorDetector(int quantidade){
        this.contDetector = quantidade;
    }
    
    public int getContadorDetector(){
        return this.contDetector;
    }
    public void setContadorMisselLinha(int quantidade){
        this.contMisselLinha = quantidade;
    }
    
    public int getContadorMisselLinha(){
        return this.contMisselLinha;
    }
    public void setContadorMisselColuna(int quantidade){
        this.contMisselColuna = quantidade;
    }
    
    public int getContadorMisselColuna(){
        return this.contMisselColuna;
    }
    
    public void ganhaPontos(){
        this.pontos = this.pontos + 100;
    }
    public void perdePontos(){
        this.pontos = this.pontos - 20;
    }
    
    public void setQuant_deBarcos(int valor){
        this.quant_deBarcos = valor;
    }
    public int getQuant_deBarcos(){
        return this.quant_deBarcos;
    }
    
    
    public int getPontos(){
        return this.pontos;
    }
    
    
}
