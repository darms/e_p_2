/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jogo;

/**
 *
 * @author darms
 */
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.util.Arrays;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class CanvasJogo extends Canvas {
	
           
        
	public static final int Largura_Canvas = 60;
	public static final int Altura_Canvas = 60;
	public static final int Margem= 0;
	
        
        
        private int numLinhas = 30;
	private int numColunas = 30;
        
        private int animationCounter = 0;
	private int animationCounterDirection = 0;
	
	private int [][] Tiro = new int[numLinhas][numColunas];
        
        
        
  
	
	@Override
	public void paint(Graphics g) {
            
            //Contador para alterar as imagens
            if(animationCounterDirection == 0) {
			if (animationCounter < 6) {
				animationCounter++;
			}
			else {
				animationCounterDirection = 1;
			}
		}
		else {
			if (animationCounter > 0) {
				animationCounter--;
			}
			else {
				animationCounterDirection = 0;
			}
		}
            
                ImageIcon agua;

                // Prepare an ImageIcon
		if(animationCounter==5 || animationCounter==4 || animationCounter==3 ){
                     agua = new ImageIcon("images/azul"+String.valueOf(animationCounter)+".png");
                } 
                
                else if(animationCounter==2 || animationCounter==0){
                     agua = new ImageIcon("images/azul"+String.valueOf(animationCounter)+".jpeg");
                } 
                
                else{
                     agua = new ImageIcon("images/azul"+String.valueOf(animationCounter)+".png");
                }
                
                //Criação dos objetos tipo imagem, exceto o da água que foi criado antes.
                ImageIcon explosao = new ImageIcon("images/tiro.jpeg");
		ImageIcon erro = new ImageIcon("images/yellow.png");
                ImageIcon iconSeeAcerto = new ImageIcon("images/yellow.png");
                ImageIcon iconSeeErro = new ImageIcon("images/purple.jpg");
                
                // Prepare an Image object to be used by drawImage()
		final Image img = agua.getImage();
		final Image imgShot = explosao.getImage();
                final Image imgErro = erro.getImage();
                final Image imgSeeErro = iconSeeErro.getImage();
                final Image imgSeeAcerto = iconSeeAcerto.getImage();
               
                //Pintura do Canvas
                for(int i = 0; i < getNumColunas(); i++) {
			for(int j = 0; j < getNumLinhas(); j++) {				
				g.drawImage(img, i*Largura_Canvas+Margem, j*Altura_Canvas+Margem, Largura_Canvas, Altura_Canvas, null);
                                
                               if(Tiro[i][j]==6){
                                   g.drawImage(imgErro, i*Largura_Canvas+Margem, j*Altura_Canvas+Margem, Largura_Canvas, Altura_Canvas, null);
                               }
                               
                               /*Tentativa falha do botao Detector 
                                  else if(Tiro[i][j]==12){
                                   g.drawImage(imgSeeErro, i*Largura_Canvas+Margem, j*Altura_Canvas+Margem, Largura_Canvas, Altura_Canvas, null);
                               }
                              
                               else if(Tiro[i][j]==13){
                                   g.drawImage(imgSeeAcerto, i*Largura_Canvas+Margem, j*Altura_Canvas+Margem, Largura_Canvas, Altura_Canvas, null);
                               }
                               */
                               else if(Tiro[i][j]==7 || Tiro[i][j]==8 || Tiro[i][j]==9 || Tiro[i][j]==10 || Tiro[i][j]==11){
                                g.drawImage(imgShot, i*Largura_Canvas+Margem, j*Altura_Canvas+Margem, Largura_Canvas, Altura_Canvas, null);
                                    
                                }
			}

		}	
                
                
                
	
        }
        
        
        
        
        public int getNumColunas() {
		return numColunas;
	}

	public void setNumColunas(int numColunas) {
		this.numColunas = numColunas;
	}

	public int getNumLinhas() {
		return numLinhas;
	}

	public void setNumLinhas(int numLinhas) {
		this.numLinhas = numLinhas;
	}
	
        public void setMatriz(int n,int m, int matriz){
            this.Tiro[n][m]= matriz;
        }
        
        public int getMatriz(int n,int m) {   
            return this.Tiro[n][m];   
        }
        
        
        
        
        
     
}